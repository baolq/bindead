package bindead.environment.platform;

import rreil.lang.MemVar;
import bindead.analyses.Bootstrap;
import bindead.domainnetwork.interfaces.RegionCtx;
import bindead.domainnetwork.interfaces.RootDomain;
import binspot.x86.x32.X32Disassembler;

/**
 * Platform for Intel x86-32bit processors using the old disassembler implemented in Java.
 */
public class LegacyX86_32Platform extends Platform {
  private static Platform INSTANCE;
  private static final String stackPointerName = "esp";
  private static final String instructionPointerName = "eip";

  private LegacyX86_32Platform () {
    super(X32Disassembler.INSTANCE);
  }

  public static Platform instance () {
    if (INSTANCE == null)
      INSTANCE = new LegacyX86_32Platform();
    return INSTANCE;
  }

  /**
   * {@inheritDoc}
   */
  @Override public Bootstrap forwardAnalysisBootstrap () {
    return new Bootstrap() {
      @Override public <D extends RootDomain<D>> D bootstrap (D state, long initialPC) {
// TODO: remove old initialization code when new version below is foolproof
//        MemVar stackPointerId = MemVar.getVarOrFresh(stackPointerName);
//        BigInt initialStackOffset = BigInt.of(4098 * 2);
//        LowLevelRReilOpnd esp = getDisassembler().translateIdentifier(stackPointerName);
//        state = state.introduceRegion(initialStackRegionId, RegionCtx.EMPTYGREEDY);
//        state = state.introduceRegion(stackPointerId, RegionCtx.EMPTYGREEDY);
        //state = state.assignSymbolicAddressOf(((Rvar) esp.toRReil()).asLhs(), initialStackRegionId);
//        RReilInstructionFactory rreil = RReilInstructionFactory.INSTANCE;
//        RReilInstruction stmt = rreil.ADD(RReilAddr.ZERO, esp, esp, rreil.immediate(esp.size(), initialStackOffset));
//        state = state.eval((RReil.Assign) stmt.toRReil(), ProgramPoint.nowhere);
        // TODO: only here for the old ROOT domain. remove when root is deleted
        state = state.introduceRegion(MemVar.getVarOrFresh("stack"), RegionCtx.EMPTYSTICKY);
        state = initializeStackPointer(state);
        state = initializeInstructionPointer(state, initialPC);
        return state;
      }
    };
  }

  @Override public String getStackPointer () {
    return stackPointerName;
  }

  @Override public String getInstructionPointer () {
    return instructionPointerName;
  }

  @Deprecated @Override public MemVar getStackRegion () {
    return MemVar.getVarOrFresh("stack.x86_32");
  }

}
