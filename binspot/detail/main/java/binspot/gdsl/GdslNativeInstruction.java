package binspot.gdsl;

import java.util.ArrayList;

import rreil.disassembler.Instruction;
import rreil.disassembler.OpcodeFormatter;
import rreil.disassembler.OperandTree;
import rreil.disassembler.OperandTree.NodeBuilder;
import rreil.disassembler.OperandTree.Type;
import binspot.INativeInstruction;

public class GdslNativeInstruction implements INativeInstruction {
  private final gdsl.decoder.NativeInstruction gdslInsn;
  private final byte[] opcode;
  private final long address;
  private final GdslOperand operands[];

  public GdslNativeInstruction (gdsl.decoder.NativeInstruction gdslInsn, byte[] opcode, long address) {
    this.gdslInsn = gdslInsn;
    this.opcode = opcode;
    this.address = address;

    operands = new GdslOperand[gdslInsn.operands()];
    for (int i = 0; i < operands.length; i++)
      operands[i] = new GdslOperand(gdslInsn, i);
  }

  @Override public Instruction toTreeInstruction () {
    ArrayList<OperandTree> operands = new ArrayList<OperandTree>();

    for (int i = 0; i < this.operands.length; i++) {
      NodeBuilder builder = new NodeBuilder();
      builder.type(Type.Op).data(this.operands[i]);
      operands.add(new OperandTree(builder.build()));
    }

    return new GdslInstruction(address, opcode, gdslInsn.mnemonic(), gdslInsn, operands);
  }

  @Override public String architecture () {
    /*
     * Todo: ...
     */
    return "gdsl";
  }

  @Override public String mnemonic () {
    return gdslInsn.mnemonic();
  }

  @Override public byte[] opcode () {
    return opcode;
  }

  @Override public long address () {
    return address;
  }

  @Override public GdslOperand operand (int idx) {
    return operands[idx];
  }

  @Override public int numberOfOperands () {
    return operands.length;
  }

  @Override public GdslOperand[] operands () {
    return operands;
  }

  @Override public int length () {
    return (int)gdslInsn.getSize()*8;
  }

  @Override public StringBuilder asString (StringBuilder pretty) {
    return pretty.append(gdslInsn.toString());
  }

  @Override public StringBuilder opcode (StringBuilder buf) {
    return OpcodeFormatter.format(opcode, buf);
  }

  @Override public StringBuilder address (StringBuilder buf) {
    return buf.append(String.format("%08x", address));
  }

}
