
#include <string.h>
#include "test.h"

char str[32] = "Foo";

int str0 ( char* str)
{
    char s[64] = "Bar";
    return (strlen (s) <= strlen (str)); 
}

int str1 (char*, char*) __attribute__((noinline));
int str1 ( char* s1, char* s2)
{
    return (strlen (s1) <= strlen (s2)); 
}

int testStr0 ()
{
    char s[32] = "Baz";
    
    return (str0 (s));
} 

int testStr1 ()
{
    char s1[32] = "Foo";
    char s2[64] = "Bar";

    return (str1 (s1, s2));
}
