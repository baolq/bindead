//@summarystart
// The SLAM example from "Automatically Validating Temporal Safety Properties of Interfaces"
// which verifies that the program calls AcquireSpinLock() iff it does not have it already,
// and calls ReleaseSpinLock() iff it has already acquired the lock.
//@summaryend
#include "../include/reach.h"

#define N 5

#define DEFAULT_INT_BITS N

int lck;
int errVar;

void AcquireSpinLock()
{
	if(lck == 0)
    lck = 1;
	else
    errVar = 1;
}

void ReleaseSpinLock()
{
	if(lck == 1)
    lck = 0;
	else
    errVar = 1;
}

int main()
{
	int nPackets, nPacketsOld, x, y;
	lck = 0;	
	errVar = 0;

  if(y < 0 || y > 10) return 1;

  do {
	  AcquireSpinLock();
	  nPacketsOld = nPackets;

    if(y > 0) {
      ReleaseSpinLock();
		  nPackets = nPackets+1;
	    y--;
    }

  } while(nPackets != nPacketsOld);

	ReleaseSpinLock();

	if(errVar == 1)
	  UNREACHABLE();
	else
    x = 8;
	
  return 0;
}

