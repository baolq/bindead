//@summarystart
// Illustrates McVeto's ability to deal with setjmp
// and longjmp.
//@summaryend
#include "../include/reach.h"

#define OFS_EBP   0
#define OFS_EBX   4
#define OFS_EDI   8
#define OFS_ESI   12
#define OFS_ESP   16
#define OFS_EIP   20

typedef struct
{
  unsigned long ebp;
  unsigned long ebx;
  unsigned long edi;
  unsigned long esi;
  unsigned long esp;
  unsigned long eip;
} jmp_buf[1];



__declspec(naked)
int csurf_replace_setjmp(jmp_buf env)
{
  __asm {
    mov edx, 4[esp]          // Get jmp_buf pointer
    mov eax, [esp]           // Save EIP
    mov OFS_EIP[edx], eax
    mov OFS_EBP[edx], ebp    // Save EBP, EBX, EDI, ESI, and ESP
    mov OFS_EBX[edx], ebx
    mov OFS_EDI[edx], edi
    mov OFS_ESI[edx], esi
    mov OFS_ESP[edx], esp
    xor eax, eax             // Return 0
    ret
  }
}

__declspec(naked)
void csurf_replace_longjmp(jmp_buf env, int value)
{
  __asm
  {
    mov edx, 4[esp]          // Get jmp_buf pointer
    mov eax, 8[esp]          // Get return value (eax)

    mov esp, OFS_ESP[edx]    // Switch to new stack position
    mov ebx, OFS_EIP[edx]    // Get new EIP value and set as return address
    mov [esp], ebx
    
    mov ebp, OFS_EBP[edx]    // Restore EBP, EBX, EDI, and ESI
    mov ebx, OFS_EBX[edx]
    mov edi, OFS_EDI[edx]
    mov esi, OFS_ESI[edx]

    ret
  }
}


static jmp_buf buf;
int x;

void first(void) {
    csurf_replace_longjmp(buf,1);             // jumps back to where setjmp was called - making setjmp now return 1
    //UNREACHABLE(); 
}
 
int main() {   
    int x;

    if (x==0) return;

    if ( ! csurf_replace_setjmp(buf) ) {
        first();                // when executed, setjmp returns 0
    } else {
        ++x;
    }

    if (x==0) {
        REACHABLE();
    }
 
    return 0;
}
