// @summarystart
// Example from the "Lazy Abstraction" (POPL'02) paper.
// The goal is to verify that the program does not call
// lock() when it already has a lock, and it never calls
// unlock() when it does not hold a lock.
// @summaryend
#include "../include/reach.h"

#define N 5
#define DEFAULT_INT_BITS N

int lck;
int errVar;

void ProcError()
{
	errVar = 1;
}

void lock()
{
  if(lck == 0)
    lck = 1;
  else
    ProcError();
}

void unlock()
{
	if(lck == 1)
    lck = 0;
	else
    ProcError();
}

int main()
{
	int get_lock, x, y1, y2, y3;
	int new_v, old_v;
	errVar = 0;

  if(y1 < 0 || y1 > 10) return 1;
  if(y2 < 0 || y2 > 10) return 1;
  if(y3 < 0 || y3 > 10) return 1;

	lck = 0;
	
  while(y2 > 0) {
		get_lock = 0;

    if(y1 > 0) {
	    lock(); 
      get_lock = get_lock+1;
      y1--;
    }

    if(get_lock > 0)
      unlock();

    y2--;
  }

  do {
    lock();
	  old_v = new_v;
    if(y3 > 0) {
      unlock(); 
      new_v = new_v+1;
      y3--;
    }
  } while(new_v != old_v);

  unlock();

  if(errVar == 1)
    UNREACHABLE();
  else
    x = 8;
  
  return 0;
}

