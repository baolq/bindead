# Bindead #

Bindead is a static analyzer for binaries (machine code) developed at the Technical University of Munich.
It features a disassembler that translates machine code bits into an assembler like language (RREIL - Relational Reverse Engineering Intermediate Language) that in turn is then analyzed by the static analysis component using abstract interpretation. The analyzer allows to reason about the runtime behavior of a program and find potential bugs. To this end we perform a collection of (numeric and symbolic) analyses on the program and are able to infer memory access bounds and various other numeric properties statically, i.e. without running the program.

A more detailed description along with publications and other material can be found on the [wiki of the project page][wiki-url]. 

## Disclaimer ##
The analyzer is still in the stage of a research prototype, missing various features. Hence, think of it as an alpha release. We are working on improving stability and scalability, however there are still lots of things to do before the analyzer may easily be used in the wild. Currently it works on small examples but bails out with exceptions on more complicated stuff (as soon as it cannot infer a jump target due to loss of precision). Adding a graceful degradation mechanism to continue an analysis even in such cases is the first thing on the TODO list.  
Furthermore, the analyzer is not a one-click solution to binary analysis. A static analysis of binaries will often require manual refinement and interpretation of the results. Especially understanding what causes the warnings produced by the analyzer will require manual inspection. Interaction with the user to provide refinements to the analysis results is still missing but is on the TODO list.


## Download and usage: ##
To download the sources clone the [Bindead repository][bindead-url] using git. Prebuild packages will be available in the download section of the site in future.

### Project Structure ###
The analyzer consists of various sub-projects that may also be used alone. Below is a brief description of the projects, 
where later ones depend on the ones mentioned before:

- JavaLX: Library for data structures (e.g. immutable collections) and tools used throughout the project
- Binparse: Parsers for executable file formats (e.g. ELF, PE) 
- RREIL: The intermediate language used in the analyzer and semantic translation from disassembled instructions
- Binspot: Disassemblers for various architectures
- Bindead: The static analyzer


### License ###
The project is open source using the GPL License. See LICENSE file for details.

### Development ###
Feel free to fork and send pull requests or report bugs in the issue tracker on the [project's site](https://bitbucket.org/mihaila/bindead/issues). The project is developed with both Eclipse and Netbeans, thus in the `resources` directory are code-style files for these two IDEs. Although untested, the project should be loadable into any IDE that integrates with the Maven build tool or use you favorite editor and build from the command line.

### Command line ###
The analyzer is usable from the command line, type `--help` for a description of the possible commands and see the [wiki][wiki-url] for a more detailed explanation of the analyzer's features.

### GUI ###
Additionally, there is a GUI built around the analyzer, that allows to inspect the results of an analysis by displaying the control flow graph of a program. See the [p9 project][p9-url].  

## Building: ##
### Dependences ###
Most dependencies are shipped with the code in the repository or downloaded during the build process. The analyzer is written in Java and is mostly self contained with some external Java dependencies. However, some optional components (Apron, GDSL) require native libraries to be installed on the system.

### Requirements: ###
The analyzer is written in Java, thus the dependencies are:

* JDK 1.7 or higher
* Maven build tool

Optional:

* [GDSL disassembler frontend](https://code.google.com/p/gdsl-toolkit/)
* [Apron library of abstract domains](http://apron.cri.ensmp.fr/library/) 

### Build process: ###

Under Unix: 

* use the shipped `build.sh` script in the top-level directory. It will build all the components and copy the resulting program to `bindead.jar` to the current directory. The jar contains everything besides the optional native dependencies. Run it using `java -jar bindead.jar <options> <file>` 

Under Windows and Unix or anything else:

* go into the top-level directory `bindead-toplevel` and install some library dependencies by issuing `mvn clean`
* compile and install the various subprojects by typing `mvn install -DskipTests` in the top-level directory
* go into the `bindead` subdirectory and build a standalone jar version of the tool using `mvn clean compile assembly:single`
* the standalone jar is placed in `bindead/target/bindead-version-...` and is usable from the command line


## Contact ##
Bogdan Mihaila <mihaila@in.tum.de>

[wiki-url]: https://bitbucket.org/mihaila/bindead/wiki/Home
[p9-url]: https://bitbucket.org/mihaila/p9
[bindead-url]: https://bitbucket.org/mihaila/bindead "Bindead page"